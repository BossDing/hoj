## 导入用户

![在这里插入图片描述](https://img-blog.csdnimg.cn/2021052322072728.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80Mzg1MzA5Nw==,size_16,color_FFFFFF,t_70)



要求如下:

1. 用户数据导入仅支持csv格式的用户数据。

2. 共三列数据: 用户名，密码，邮箱，任一列不能为空，否则该行数据可能导入失败。

3. 第一行不必写(“用户名”，“密码”，“邮箱”)这三个列名。

4. 请导入保存为UTF-8编码的文件，否则中文可能会乱码。

