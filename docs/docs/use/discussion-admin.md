# 评论管理

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210523223605573.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80Mzg1MzA5Nw==,size_16,color_FFFFFF,t_70#pic_center)



- 后台管理员可以查看所有的讨论帖，并且可以选择是否置顶，是否正常显示，删除，查看等

- 后台管理员可以查看对应讨论帖的举报内容

  ![在这里插入图片描述](https://img-blog.csdnimg.cn/20210523221321418.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80Mzg1MzA5Nw==,size_16,color_FFFFFF,t_70)

