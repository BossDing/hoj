---
home: true
heroImage: /img/logo.png
heroText: HOJ
tagline: 基于分布式、前后端分离的高性能在线评测系统
actionText: 快速了解 →
actionLink: /introducition/
features:
- title: 分布式
  details: 支持多台判题服务随时增减
- title: 高效化
  details: 采用前后端分离，开发迅速，使用高性能可复用判题沙盒
- title: 定制化
  details: 网站高度集中配置，支持定制化修改
- title: 安全化
  details: 判题使用 cgroup 隔离用户程序，网站权限控制完善
- title: 多样化
  details: 独有自身判题服务，同时支持其它知名OJ题目的提交判题
footer: MIT Licensed | Copyright © 2021.09.21 @Author Himit_ZH  QQ Group:598587305
---