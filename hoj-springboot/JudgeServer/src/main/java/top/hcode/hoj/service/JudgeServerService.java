package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.JudgeServer;

public interface JudgeServerService extends IService<JudgeServer> {

}
