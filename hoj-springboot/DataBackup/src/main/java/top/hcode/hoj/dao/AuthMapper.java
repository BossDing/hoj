package top.hcode.hoj.dao;

import top.hcode.hoj.pojo.entity.Auth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Himit_ZH
 * @since 2020-10-23
 */
public interface AuthMapper extends BaseMapper<Auth> {

}
