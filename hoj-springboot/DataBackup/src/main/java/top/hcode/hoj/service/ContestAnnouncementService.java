package top.hcode.hoj.service;


import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.ContestAnnouncement;

public interface ContestAnnouncementService extends IService<ContestAnnouncement> {
}
