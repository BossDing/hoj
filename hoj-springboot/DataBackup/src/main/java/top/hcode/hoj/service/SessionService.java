package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.Session;

public interface SessionService extends IService<Session> {
    public void checkRemoteLogin(String uid);
}
