package top.hcode.hoj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.hcode.hoj.dao.ProblemLanguageMapper;
import top.hcode.hoj.pojo.entity.ProblemLanguage;
import top.hcode.hoj.service.ProblemLanguageService;

/**
 * @Author: Himit_ZH
 * @Date: 2020/12/13 00:04
 * @Description:
 */
@Service
public class ProblemLanguageServiceImpl  extends ServiceImpl<ProblemLanguageMapper, ProblemLanguage> implements ProblemLanguageService {
}