package top.hcode.hoj.service.impl;

import top.hcode.hoj.pojo.entity.ContestScore;
import top.hcode.hoj.dao.ContestScoreMapper;
import top.hcode.hoj.service.ContestScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Himit_ZH
 * @since 2020-10-23
 */
@Service
public class ContestScoreServiceImpl extends ServiceImpl<ContestScoreMapper, ContestScore> implements ContestScoreService {

}
