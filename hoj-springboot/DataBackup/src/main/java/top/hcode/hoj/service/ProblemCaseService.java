package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.ProblemCase;

/**
 * @Author: Himit_ZH
 * @Date: 2020/12/14 19:58
 * @Description:
 */
public interface ProblemCaseService extends IService<ProblemCase> {
}