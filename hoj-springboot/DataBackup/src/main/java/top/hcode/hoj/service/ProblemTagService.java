package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.ProblemTag;

public interface ProblemTagService extends IService<ProblemTag> {
}
