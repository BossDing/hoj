package top.hcode.hoj.service;


import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.DiscussionReport;

public interface DiscussionReportService extends IService<DiscussionReport> {
}
