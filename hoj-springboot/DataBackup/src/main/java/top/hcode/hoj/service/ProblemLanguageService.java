package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.ProblemLanguage;

/**
 * @Author: Himit_ZH
 * @Date: 2020/12/13 00:03
 * @Description:
 */
public interface ProblemLanguageService extends IService<ProblemLanguage> {
}