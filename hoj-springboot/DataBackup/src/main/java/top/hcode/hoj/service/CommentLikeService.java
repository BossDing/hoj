package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.CommentLike;


public interface CommentLikeService extends IService<CommentLike> {
}
