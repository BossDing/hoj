package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.Reply;

/**
 * @Author: Himit_ZH
 * @Date: 2021/5/5 22:08
 * @Description:
 */
public interface ReplyService extends IService<Reply> {
}