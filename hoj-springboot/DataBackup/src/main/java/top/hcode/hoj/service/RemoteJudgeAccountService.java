package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.RemoteJudgeAccount;

public interface RemoteJudgeAccountService extends IService<RemoteJudgeAccount> {
}
