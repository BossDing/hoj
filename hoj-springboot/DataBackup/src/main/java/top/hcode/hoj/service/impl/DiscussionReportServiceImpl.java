package top.hcode.hoj.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.hcode.hoj.dao.DiscussionReportMapper;
import top.hcode.hoj.pojo.entity.DiscussionReport;
import top.hcode.hoj.service.DiscussionReportService;

/**
 * @Author: Himit_ZH
 * @Date: 2021/5/11 21:46
 * @Description:
 */
@Service
public class DiscussionReportServiceImpl extends ServiceImpl<DiscussionReportMapper, DiscussionReport> implements DiscussionReportService {
}