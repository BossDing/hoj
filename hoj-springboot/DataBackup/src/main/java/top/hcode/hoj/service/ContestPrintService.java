package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.ContestPrint;

/**
 * @Author: Himit_ZH
 * @Date: 2021/9/19 21:05
 * @Description:
 */
public interface ContestPrintService extends IService<ContestPrint> {
}