package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.CodeTemplate;

public interface CodeTemplateService extends IService<CodeTemplate> {
}
