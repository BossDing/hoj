package top.hcode.hoj.service.impl;

import top.hcode.hoj.pojo.entity.ContestRegister;
import top.hcode.hoj.dao.ContestRegisterMapper;
import top.hcode.hoj.service.ContestRegisterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Himit_ZH
 * @since 2020-10-23
 */
@Service
public class ContestRegisterServiceImpl extends ServiceImpl<ContestRegisterMapper, ContestRegister> implements ContestRegisterService {

}
