package top.hcode.hoj.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.hcode.hoj.pojo.entity.DiscussionLike;

public interface DiscussionLikeService extends IService<DiscussionLike> {
}
